FROM openjdk:8-jdk-alpine
ADD ./target/dynamic-form-0.0.1-SNAPSHOT.jar dynamic-form.jar
ADD ./dynamic-form.sh dynamic-form.sh
RUN ["chmod", "+x", "/dynamic-form.sh"]
ENTRYPOINT ["/dynamic-form.sh"]