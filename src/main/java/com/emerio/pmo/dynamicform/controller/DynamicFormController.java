package com.emerio.pmo.dynamicform.controller;

import com.emerio.pmo.dynamicform.service.DynamicFormService;

import org.bson.Document;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import reactor.core.publisher.Mono;

@RestController
public class DynamicFormController
{

    @Autowired
    private DynamicFormService dynamicFormService;

    @RequestMapping(value = "/rest/dynamic-form/getall/{formcode}", method = RequestMethod.POST)
    public Mono<Document> getAll(@PathVariable String formcode,
                                @RequestBody String body,
                                @RequestParam(name = "limit" , required = false) String limit,
                                @RequestParam(name = "offset" , required = false) String offset,
                                @RequestParam(name = "sort" , required = false, defaultValue = "") String sort,
                                @RequestParam(name = "sortBy" , required = false , defaultValue = "") String sortBy)
    {
        Document payload = new Document();
        payload.append("formcode", formcode);

        try 
        {
            int intLimit = Integer.parseInt(limit);
            payload.append("limit", intLimit);
        } catch (Exception e)
        {
            payload.append("limit", 0);
        }

        try 
        {
            int intOffset = Integer.parseInt(offset);
            payload.append("offset", intOffset);
        } catch (Exception e)
        {
            payload.append("offset", -1);
        }

        try 
        {
            int intSortBy = Integer.parseInt(sortBy);
            Document sorting = new Document(sort, intSortBy);
            payload.append("sort", sorting);

        } catch (Exception e)
        {
            payload.append("sort", "sort not available");
        }

        payload.append("filterList", body);

        return Mono.just(dynamicFormService.getAllService(payload));
    }
}