package com.emerio.pmo.dynamicform.service;

import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;
import com.mongodb.client.MongoCollection;

import static com.mongodb.client.model.Sorts.*;
import static com.mongodb.client.model.Filters.*;

import java.util.ArrayList;
import java.util.List;

import org.bson.Document;
import org.bson.conversions.Bson;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class MongoService
{

    @Value("${app.mongodb.host}")
    private String host;

    @Value("${app.mongodb.port}")
    private String port;

    private MongoCollection getCollection(String database, String collection)
    {
        MongoClient client=MongoClients.create("mongodb://"+this.host+":"+this.port);;
        // try
        // {
            return client.getDatabase(database).getCollection(collection);
        // } finally
        // {
        //     client.close();
        // }
    }

    public Document insertOneDocument(String database, String collection, Document insertedData)
    {
        MongoCollection mongoCollection = this.getCollection(database, collection);
        mongoCollection.insertOne(insertedData);
        return (Document) mongoCollection.find(eq(insertedData.getObjectId("_id"))).first();
    }

    public List<Document> aggregateDocument(String database, String collection, List<Bson> aggregates)
    {
        return (List<Document>) this.getCollection(database, collection).aggregate(aggregates).into(new ArrayList<Document>());
    }

    public Document getOneDocument(String database, String collection, Bson filter, Bson projection)
    {
        return (Document) this.getCollection(database, collection).find(filter).projection(projection).first();
    }

    public List<Document> getAllDocument(String database, String collection, Bson filter, Bson projection)
    {
        return (List<Document>) this.getCollection(database, collection).find(filter).projection(projection).into(new ArrayList<Document>());   
    }

    
}