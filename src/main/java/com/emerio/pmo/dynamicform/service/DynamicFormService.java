package com.emerio.pmo.dynamicform.service;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static com.mongodb.client.model.Projections.*;
import static com.mongodb.client.model.Filters.*;

import org.bson.Document;
import org.bson.conversions.Bson;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class DynamicFormService
{

    
    @Value("${app.mongodb.database}")
    private String database;

    @Value("${app.mongodb.collection}")
    private String collection;

    // @Value("${app.mongodb.database-exclude}")
    // private String databaseExclude;

    // @Value("${app.mongodb.collection-exclude}")
    // private String collectionExclude;

    @Autowired
    private MongoService mongosService;


    public Document getAllService(Document payload)
    {
        Document result = new Document();

        System.out.println("==== payload : " + payload.toJson());

        Bson filter = eq("formcode", payload.getString("formcode"));

        Document form = mongosService.getOneDocument(database, collection, filter, excludeId());
        // System.out.println("==== form : " + form);
        // Document x = (Document) form.get("excludedField");
        // System.out.println("==== x : " + x);
        // List<String> xx =(List<String>) x.get(form.getString("componentcollection"));
        // System.out.println("=== xx : " + xx);
        List<Document> listOfComponents = mongosService.getAllDocument(form.getString("componentdatabase"), form.getString("componentcollection"), filter, exclude((List<String>) ((Document) ((Document) form.get("excludedField")).get(form.getString("componentcollection"))).get("queryexclude")));
        // List<Document> listOfComponents = mongosService.getAllDocument(form.getString("componentdatabase"), form.getString("componentcollection"), filter, excludeId());

        doConvertTree(listOfComponents, form);

        // excludedFields(listOfComponents, form);

        result.append("dataset", listOfComponents);

        System.out.println("==== result : " + result.toJson());

        return result;
    }
    
    private void removeAfterUsedFields(Document index, List<String> removedList)
    {
        for (String indexList : removedList)
        {
            if (index.containsKey(indexList))
            {
                index.remove(indexList);
            }
        }
    }

    public void doConvertTree(List<Document> value, Document form)
    {

        for (Document index : value)
        {
            if (index.containsKey("components"))
            {
                doConvertTree((List<Document>)index.get("components"), form);
            } else
            {
                List<Document> listOfDatas = mongosService.getAllDocument(index.getString("valuedetaildatabase"), index.getString("valuedetailcollection"), eq("componentcode", index.getString("componentcode")), exclude((List<String>) ((Document) ((Document) form.get("excludedField")).get(index.getString("valuedetailcollection"))).get("queryexclude")));
                
                for (Document indexDatas : listOfDatas)
                {
                    removeAfterUsedFields(indexDatas, (List<String>) ((Document) ((Document) form.get("excludedField")).get(index.getString("valuedetailcollection"))).get("removeafterused"));
                }

                index.append("data", listOfDatas);

                
                removeAfterUsedFields(index, (List<String>) ((Document) ((Document) form.get("excludedField")).get(form.getString("componentcollection"))).get("removeafterused"));
                
            }
        }
    }

    private Bson doFilter(String value)
    {
        List<Bson> listOfFilter = new ArrayList<>();
        Document filter = Document.parse(value);

        for (Map.Entry<String, Object> index : filter.entrySet())
        {
            listOfFilter.add(index.getValue().getClass() == String.class ? regex(index.getKey(), ".*"+index.getValue()+".*") : eq(index.getKey(), index.getValue()));
        }

        System.out.println("==== listOfFilter : " + listOfFilter);

        return and(listOfFilter);
    }
}